#include <stdio.h>
#include <ctype.h>

//https://reurl.cc/dVN382
//create a new datatype called student
struct student{
  int id;
  char name[10];
};

int main(){
    struct student john = {291, {'j', 'o', 'h', 'n', '\0'}};
    printf("%d\n",john.id);
    printf("%s\n",john.name);

    john.id+=1000;
    for(int i=0;john.name[i]!='\0';i++){
        john.name[i]=toupper(john.name[i]);
    }
    printf("%d\n",john.id);
    printf("%s\n",john.name);
    return 0;
}

