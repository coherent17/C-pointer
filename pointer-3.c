#include <stdio.h>
#include <stdlib.h>
#define SIZE 1000000
/*int main( void )
{
   int i;
   int n[ SIZE ];

   for ( i = 0; i < SIZE; i++ ) {
      n[ i ] = rand();
   }
   return 0;
}*/
//Dynamic memory allocation
int main(){
   int i;
   int *n = (int *)malloc(SIZE * sizeof(int));//sizeof(int)=4
   for (i=0;i<SIZE;i++){
      n[i]=rand();
   }
   //free:
   //deallocates memory allocated by malloc
   //takes pointer as an argument
   free(n);
   return 0;
}

