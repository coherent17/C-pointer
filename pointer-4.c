#include <stdio.h>
int main(){
    int a;
    int *aptr;

    a=7;
    aptr=&a;

    printf("The address of a is %p\n",&a); //0061FF1C
    printf("The value of aptr is %p\n",aptr);//0061FF1C
    printf("The value of a is %d\n",a);//7
    printf("The value of *aptr is %d\n",*aptr);//7
    //showing that * and & are complements of each other
    printf("&*aptr=%p\n",&*aptr);//0061FF1C
    printf("*&aptr=%p\n",*&aptr);//0061FF1C
    return 0;
}
