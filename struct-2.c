#include <stdio.h>
#include <ctype.h>

//when editting the data in the function it will not change the outer value
//if deliver the struct into the function, it actually copy the struct into the function instead of change it
struct student{
    int id;
    char name[10];
};

void new_student(struct student new_one){
    new_one.id+=1000;
    for(int i=0;new_one.name[i]!='\0';i++){
        new_one.name[i]=toupper(new_one.name[i]);
    }
    printf("%d\n",new_one.id);//1291
    printf("%s\n",new_one.name);//JOHN
}
int main(){
    struct student john={291,{'j','o','h','n','\0'}};
    new_student(john);
    printf("%d\n",john.id);//291
    printf("%s\n",john.name);//john
    return 0;
}
