#include <stdio.h>

int cubeByValue(int);
void cubeByReference(int*);

int main(){
    int number=5;
    printf("%d\n",number);//5
    number=cubeByValue(number);
    printf("%d\n",number);//125
    number=5;
    cubeByReference(&number);
    printf("%d\n",number);//125
    return 0;
}

//call by value
int cubeByValue(int n){
    return n*n*n;
}

//call by reference
void cubeByReference(int *nptr){
    *nptr=*nptr * *nptr * *nptr;
}
