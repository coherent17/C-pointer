//strchr():from begin to search
#include <stdio.h>
#include <string.h>//need to include this headfile

int main(){
    char s1[]="ATAGCTACTG";
    char *ptr;
    ptr=&s1;
    printf("initial s1 address:%p\n",ptr);
    //initial ptr value is the address of s1
    while(ptr!=NULL){
        ptr=strchr(s1,'T');//return the address of the T value to ptr
        printf("%p\n",ptr);
        if(ptr!=0){
            *ptr='U';//use pointer to change T to U
        }
    }
    printf("%s\n",s1);//ATAGCTACTG
    return 0;
}
//result:
/*initial s1 address:0061FF11
0061FF12
0061FF16
0061FF19
00000000
AUAGCUACUG*/
