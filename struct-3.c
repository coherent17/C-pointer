#include <stdio.h>

struct Ball{
    char color[10];
    double radius;
};

int main(){
    struct Ball ball={"red",4.0};
    struct Ball *ptr;
    ptr=&ball;

    printf("%s %.2f\n",ptr->color,ptr->radius);
    return 0;
}
//red 4.00
