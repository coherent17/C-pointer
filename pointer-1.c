#include <stdio.h>
//https://reurl.cc/zb3j1e

int main(){
    int b=2;
    printf("%d\n",b);  //the value of the variable b:2
    printf("%p\n",&b); //the address of the variable (in Hex):0061FF1C
    printf("%d\n",*&b);//get the value of b from the address by(*):2

    //pointer variable: the variable to store the pointer
    int *pointer;//acclaim the pointer variable int*:the initial b variable is int
    pointer=&b;
    printf("%p\n",pointer);//0061FF1C
    printf("%p\n",&pointer);//0061FF18
    printf("%d\n",*pointer);//*pointer is b and the value is 2
    *pointer=100;//if I change the value of *pointer
    printf("%d\n",*pointer);//100
    printf("%d\n",b);//100 b will change as the *pointer change
    printf("%p\n",&pointer);//0061FF18
    printf("%p\n",pointer);//0061FF1C
    //example: variable address     value    name of the variable
    //             0061FF1C           2              b
    //             0061FF18       0061FF1C   pointer(pointer variable of b)
    return 0;
}
