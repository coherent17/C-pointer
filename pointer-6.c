#include <stdio.h>
#include <ctype.h>

void convertToUppercase(char *);

int main(){
    char string[]="character and $32.98";
    printf("%s\n",string);
    convertToUppercase(string);
    printf("%s\n",string);
    return 0;
}

void convertToUppercase(char *sptr){
    while(*sptr!='\0'){
        if(islower(*sptr)){
            *sptr=toupper(*sptr);
        }
        sptr++;
    }
}
