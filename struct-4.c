#include <stdio.h>

struct Ball{
    char color[10];
    double radius;
};

void show(struct Ball *ball);

int main(){
    struct Ball ball={"red",4.0};
	show(&ball);
	return 0;
}

void show(struct Ball *ball){
	printf("%s %.2f\n",ball->color,ball->radius);
}
//red 4.00
