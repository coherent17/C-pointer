#include <stdio.h>
#define SIZE 10

void bubbleSort(int *,const int);
int main(){
    int a[SIZE]={2,6,4,8,10,12,89,68,45,37};
    int i;
    printf("Before:\n");
    for(i=0;i<SIZE;i++){
        printf("%4d",a[i]);
    }
    printf("\n");
    bubbleSort(a,SIZE);
    printf("After:\n");
    for(i=0;i<SIZE;i++){
        printf("%4d",a[i]);
    }
    return 0;
}

void bubbleSort(int *array,const int size){
    void swap(int *, int *);
    int i,j;
    for(i=0;i<size-1;i++){
        for(j=0;j<size-1;j++){
            if(array[j]>array[j+1]){
                swap(&array[j],&array[j+1]);
            }
        }
    }
}

void swap(int *a, int *b){
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
/* 
Before:
   2   6   4   8  10  12  89  68  45  37
After:
   2   4   6   8  10  12  37  45  68  89
   */ 
