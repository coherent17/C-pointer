#include <stdio.h>
//using pointer to change the two value
int main(){
    int a,b,temp;
    int *p1,*p2;
    printf("please input the value of a:");
    scanf("%d",&a);
    printf("please input the value of b:");
    scanf("%d",&b);

    p1=&a;
    p2=&b;

    temp=*p1;
    *p1=*p2;
    *p2=temp;
    printf("the value of *p1 is:%d\n",*p1);
    printf("the value of a is:%d\n",a);
    printf("the value of *p2 is:%d\n",*p2);
    printf("the value of b is:%d\n",b);
    return 0;
}
